#!/bin/bash
#    Appends your public ssh key to your GitLab.com account.
#    Copyright (C) 2015 Christoph "criztovyl" Schulz
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Based on https://github.com/criztovyl/ssh-copy-id-github which is
#    based on https://gitlab.com/debugish/env/blob/master/scripts/auth_hosts [WTFPL License](http://wtfpl.net)

###
# Help
[ "$1" == "--help" ] || [ "$1" == "-h" ] || [ "$1" == "help" ] &&
{
    echo "Usage: ./ssh-copy-id-gitlab [username]"
    echo "Adds .ssh/id_rsa.pub to your GitLab's SSH keys."

    echo "Usage: ./ssh-copy-id-gitlab [username] [pub_key_file]"
    echo "Adds specified Public Key File to your GitLab's SSH keys."

    echo "With confirmation, non-exiting Public Key File kicks off ssh-keygen"
    exit
}

###
# Constants
TRUE=0
FALSE=1
# GitLab 2FA, currently not required when using /oauth/token with username & password.
# https://gitlab.com/gitlab-org/gitlab-ce/issues/2979
#XGL="" 
DEFAULT_KEY="$HOME/.ssh/id_rsa.pub"
GILA_APP_ID="87053d3ad5f35e427887e499ad076def5aa42af0590d58f718fb960fa669c47b"
GILA_APP_SECRET="96acaaef3141c2fd87b87d6f7acfe0852cd1367933743752cae3d5b977dd2e9e"
TOKEN_REGEX='access_token":"([[:alnum:]]*)".*'

###
# Function
# Args: username
#   username: gitlab username
#   ssh_key : SSH key file, default: $HOME/.ssh/id_rsa.pub
ssh_copy_id_gitlab() {
    

    username="$1"
    key_file="$2"

    [ -z "$key_file" ] && { key_file="$DEFAULT_KEY"; }

    if [ ! -e "$key_file" ]; then

      read -p "SSH key file doesn't exist: $key_file, do you want to generate a $key_file (y/n)?: "; echo

      if [[ "$REPLY" =~ ^[Yy]$ ]]; then
        ssh-keygen -f `echo ${key_file%.pub}`
      else
        echo "Need SSH key file to upload, e.g. $DEFAULT_KEY"
        exit 1;
      fi

    fi

    key=`cat "$key_file"`

    [ -z $username ] && read -p "GitLab.com username: " username || { username=$username; echo "Username: $username"; }

    read -sp "GitLab password: " password && echo

    # Authorize
    response=`curl -si https://gitlab.com/oauth/token -d "client_id=$GILA_APP_ID&client_secret=$GILA_APP_SECRET&grant_type=password&username=$username&password=$password"`
    [[ "$response" =~ $TOKEN_REGEX ]] && { token=${BASH_REMATCH[1]}; } || { echo "Error: No token found!"; exit 4; }

    # 2FA not required yet (OTP is 2FA in this case)
    #otp_required "$response" otp
    #otp_type "$response" "type" # app or sms

    [ `echo "$response" | grep 'Status: 401\|="The provided authorization grant is invalid, ' | wc -l` -eq 2 ] && { echo "Wrong password."; exit 5; }

    [ `echo "$response" | grep 'Status: 422\|key is already in use' | wc -l` -eq 2 ] && { echo "Key is already uploaded."; exit 5; }

    # Display raw response for unkown 400 messages
    [ `echo $response | grep 'Status: 4[0-9][0-9]' | wc -l` -eq 1 ] && { echo "Error: $response"; exit 1; }

    # No 2FA
    #if [ "$otp" == "$TRUE"  ]; then
        #read -sp "Enter your OTP code (check your $type): " code && echo

        #response=`curl -si https://api.gitlab.com/user/keys -X POST -u "$username:$password" -H "X-GitHub-OTP: $code" -H "application/json" -d "{\"title\": \"$USER@$HOSTNAME\", \"key\": \"$key\"}" | grep 'Status: [45][0-9]\{2\}\|X-GitHub-OTP: required; .\+\|message\|key' | tr -d "\r"`

        #otp_required "$response" otp
        #[ "$otp"  ==  "$TRUE" ] && { echo "Wrong OTP."; exit 10; }
        #[ `echo "$response" | grep "key" | wc -l` -gt 0 ] && echo "Success."
    #fi

    # Upload key
    response=`curl -si https://gitlab.com/api/v3/user/keys?access_token=$token -d "title=$USER@$HOSTNAME&key=$(cat $key_file)"`

    [ `echo "$response" | grep 'Status: 201 Created' | wc -l` -eq 1 ] && { echo "Success."; exit 0; }
    [ `echo "$response" | grep 'Status: 400 Bad Request\|"fingerprint":["has already been taken","cannot be generated"]' | wc -l` -eq 1 ] && { echo "Error: The GitLab server can't handle your ssh key. Please add manually."; exit 6; }
    [ `echo "$response" | grep 'Status: 400 Bad Request' | wc -l` -eq 1 ] && { echo "Request failed: $(echo "$response" | tail -n1)"; exit 7; }

    [ `echo $response | grep 'Status: 4[0-9][0-9]' | wc -l` -eq 1 ] && { echo "Error: $(echo "$response" | tail -n1)"; exit 1; }

}

#otp_required(){
    #local filteredResponse=$1
    #local resultVar=$2
    #local _otp=`echo $filteredResponse | grep "$XGL" | wc -l`
    #[ $_otp -eq 1 ] && eval $resultVar=$TRUE || eval $resultVar=$FALSE
#}
#otp_type(){
    #local filteredResponse=$1
    #local resultVar=$2
    #local _type=`echo $filteredResponse | grep "$XGL" | sed "s/.\+$XGL\(\w\+\).\+/\1/"`
    #eval $resultVar=$_type
#}
# Execute.
ssh_copy_id_gitlab "$1" "$2"
